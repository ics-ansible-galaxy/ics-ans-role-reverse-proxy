ics-ans-role-reverse-proxy
===================

Ansible role to install reverse-proxy.
Sets path based reverse proxy.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
# set to true in order to enable ssl
enable_https: fals

path_to_nginx_dir: /etc/nginx/
path_to_nginx_ssl: "{{ path_to_nginx_dir }}ssl"
path_to_nginx_public: "{{ path_to_nginx_ssl }}/nginx.pem"
path_to_nginx_crt: "{{ path_to_nginx_ssl }}nginx.crt"
path_to_nginx_csr: "{{ path_to_nginx_ssl }}nginx.csr"
path_to_nginx_dh: "{{ path_to_nginx_ssl }}dhparam.pem"
path_to_nginx_private: "{{ path_to_nginx_ssl }}nginx.key"

# set to true to regenerate certificates
force_certificates: false

# The following entries will be proxied according to the path. 
# Examples: http://hostname/ts2/
# and using http://hostname/lcr2/

reverse_proxy_path:
   - path: ts2
     backend: http://172.16.6.1
   - path: lcr
     backend: http://172.16.14.14:8080

# The following entries will be proxied according to virtualhost. 
# Examples: http://hostname/ts2/
# and using http://hostname/lcr2/

reverse_proxy_host:
  - node: hostname1.domain.com
    backend: https://realserver1.dddd.eu
  - node: hostname2.domain.com
    backend: http://realserver2.dddd.eu:8443
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-reverse-proxy
```

License
-------

BSD 2-clause
---
